# Array Methods #

## flat ##

1. Parameter (optional) : depth (number) - specifies maximum recursion depth. Default value is 1 if not specified.

2. Return: returns new array of sub-array elements concatenated to it recursively upto the specified depth.

3. Example:

        let numbers = [1, [2], [[3]]];
        // Depth is 2.
        numbers.flat(2); //[1,2,3]
        // Depth not specified.
        numbers.flat(); //[1,2,[3]]
        numbers.flat(Infinity);//[1,2,3]

4. flat() accepts optional parameter i.e. depth and returns a new array with all the elements flattened recursively according to the specified depth. If not specified, default depth value is 1. If depth is specified as Infinity then it will flatten the array elements until all the sub-array elements are not array anymore.

5. It doesn't mutate the original array.

## push ##

1. Parameter: element to push (any number of elements).

2. Return: Returns length of updated array.

3. Examples:

        const arr1 =[1,2,3,4];
        console.log(arr1.push(5,6,8)); //7
        console.log(arr1); //[1, 2, 3, 4,5, 6, 8]
        const arr1 =[1,2,3,4];
        arr1.push([5,6])
        console.log(arr1); //[ 1, 2, 3, 4, [ 5, 6 ] ]
        const arr1 =[1,2,3,4];
        arr1.push([5,[6]]);
        console.log(arr1); //[ 1, 2, 3, 4, [ 5, [ 6 ] ] ]

4. push() accepts element (or elements) as parameter and adds those at the end of the array.

5. It does mutates the original array.

## indexOf ##

1. Parameter:

    1. element (could be any - string, numbers, boolean)
    2. fromIndex(integer) (optional)

2. Return: Returns firstIndex (number) of element otherwise -1 if not found.

3. Example:

        Examples:
        const arr1 =[1,2,3,4];
        console.log(arr1.indexOf(3)); //2
        const arr1 =[1,2,3,true, true];
        console.log(arr1.indexOf(true)); //3
        const arr1 = [1,2,3,4,5,2];
        console.log(arr1.indexOf(2, 3)); //5

4. indexOf () accepts element and optional fromIndex as parameters and returns first index of the element if found, else -1.

5. It doesn't mutates the original array.

## lastIndexOf ##

1. Parameters:

    1. element - string, boolean, number
    2. fromIndex (integer) (optional)

2. Return: Returns lastIndex (number) of element otherwise -1.

3. Example:

        const arr1 =[1,2,3,4];
        console.log(arr1.lastIndexOf(3)); //2
        const arr1 =[1,2,3,true, true];
        console.log(arr1.lastIndexOf(true)); //4
        const arr1 = [1,2,3,4,5,2];
        console.log(arr1.indexOf(2)); //5

4. lastIndexOf () accepts element and optional fromIndex as parameters and returns last index of the element if found, else -1.

5. It doesn't mutates the original array.

## includes ##

1. Parameter:

    1. element - string, numbers, boolean
    2. fromIndex (integer) (optional)

2. Return: Returns true if array contains the specified element else false.

3. Examples:

        const arr1 = [1,2,3,4,5,2];
        console.log(arr1.includes(2)); //true
        const arr1 = [1,2,3,4,5,2];
        console.log(arr1.includes(6)); //false
        const arr1 = [1,2,3,4,5,2];
        console.log(arr1.includes(2, 2)); //true

4. includes () accepts element to search and optional fromIndex as parameters and returns true if the element is found else false.

5. It doesn't mutates the original array.

## reverse ##

1. Parameter: None.

2. Return: Returns reversed array.

3. Examples:

        const arr1 = [1,2,3,4,5,2];
        console.log(arr1.reverse()); //[ 2, 5, 4, 3, 2, 1 ]
        const arr1 = [1,2,3,4,5,[1,2]];
        console.log(arr1.reverse()); //[ [ 1, 2 ], 5, 4, 3, 2, 1 ]
        const arr1 = [true, false];
        console.log(arr1.reverse()); //[ false, true ]

4. reverse () reverses the elements in an array in place. This method mutates the array and returns a reference to the same array.

5. It does mutates the original array.

## every ##

1. Parameters:

    1. Function - This function is called against each element. Used for testing elements and returns true or false according to the specified condition. This function accepts up to three arguments i.e.

        - element - The current element being processed in the array.
        - index (number) - The index of current element.
        - array - The array for which every() was called upon.

    2. (optional) thisArg - A value to use as this when executing callback function. Default is undefined.

2. Return: Returns true if all elements passes the test call back function else false.

3. Example:

        const arr1 = [1,2,3,4,5,6,7];
        function big(element, index, array){
        return element>0;
        }
        console.log(arr1.every(big)); //true
        const isSubset = (array1, array2) =>
        array2.every((element) => array1.includes(element));
        console.log(isSubset([1, 2, 3, 4, 5, 6, 7], [5, 7, 6])); // true

4. every () accepts a callback function and an optional thisArg as parameters. It tests if all elements pass the given condition and if any element doesn't pass it returns false else returns true.

5. It doesn't mutates the original array.

## shift ##

1. Parameter: None

2. Return: Returns element that is removed from array else undefined if array is empty.

3. Examples:

        arr = [1,2,4,5];
        console.log(arr.shift()); //1
        console.log(arr); //[2,4,5]
        arr = [];
        console.log(arr.shift()); //undefined
        console.log(arr); //[]
        arr = [6,2,4,5];
        console.log(arr.shift()); //6
        console.log(arr); //[2,4,5]

4. shift () removes the first element from an array and returns it. If the array is empty, undefined is returned.

5. It does mutates the original array.

## splice ##

1. Parameters:

    1. start (number)
    2. deletecount (number)
    3. item-{n}

2. Return: Returns array containing deleted elements.

3. Examples:

        arr = [1,2,3,4,5,6]
        console.log(arr.splice(2,4)); // [3,4,5,6]
        console.log(arr); // [1,2]
        arr = [];
        console.log(arr.splice()); //[]
        console.log(arr); //[]
        arr = [6,2,4,5];
        console.log(arr.splice()); //[6,2,4,5]
        console.log(arr); //[6,2,4,5]

4. splice () accepts three parameters as mentioned above. Removes elements from an array and, if necessary, inserts new elements in their place, returning the deleted elements.

5. It does mutates the original array.

## find ##

1. Parameters:

    1. Function - This function is called against each element. Used to check for matching element and returns true if found else false. This function accepts up to three arguments i.e.

        - element - The current element being processed in the array.
        - index (number) - The index of current element.
        - array - The array for which find() was called upon.

    2. (optional) thisArg - A value to use as this when executing callback function.

2. Return: Returns true if matching element is found else returns undefined.

3. Example:

        arr = [1,2,3,4,5,6]
        console.log(arr.find(element => element>1)); //2
        arr = [1,2,3,4,5,6]
        console.log(arr.find(element => element<1)); //undefined
        arr = [6,8,10,7,18,20]
        function isOdd(element, index, arr){
        if (element%2 !== 0){
        return element>1;
        }
        }
        console.log(arr.find(isOdd)); //7

4. find () accepts a callback function and an optional thisArg as parameters. It finds the first element that passes the testing function, if no element passes the testing function then it returns undefined.

5. It doesn't mutate the original array.

## unshift ##

1. Parameter: elements - elements to insert at start of the array.

2. Return: Returns updated length (number) of the array.

3. Example:

        arr = [6,8,10,7,18,20]
        console.log(arr.unshift(1,2)); //8
        console.log(arr); //[1,2,6,8,10,7,18,20]
        arr = [6,8,10,7,18,20]
        console.log(arr.unshift()); //6
        console.log(arr); //[6,8,10,7,18,20]

4. unshift () accepts element/ elements as parameter. It inserts elements at the starting of the array and return length of updated array.

5. It does mutates the original array.

## findIndex ##

1. Parameters:

    1. Function - This function is called against each element. Returns true if an element satisfies the condition otherwise returns false. This function accepts up to three arguments i.e.

        - element - The current element being processed in the array.
        - index - The index of current element.
        - array - The array for which findIndex() was called upon.

    2. (optional) thisArg - A value to use as this when executing callback function. Default value is undefined.

2. Return: Returns index of the first matching element that satisfies that specified testing function. If no elements satisfies the testing function then it returns -1.

3. Example:

        const array = [1,8,4,2];
        let divisibleBy4 = (element, index, array) => element % 4 === 0;
        array.findIndex(divisibleBy4); // 1 (index of element 8)

        const array1 = [1,2,3];
        array1.findIndex(divisibleBy4); // -1

        array.findIndex((element) => element % 2 === 0); // 1

4. findIndex () accepts function and optional thisArg as parameters. Returns the index of the first element in the array which satisfies the specified testing function and -1 otherwise.

5. It doesn't mutate the original array.

## filter ##

1. Parameters:

    1. Function - This function is called against each element. Returns true if an element satisfies the condition otherwise returns false. This function accepts up to three arguments i.e.

        - element - The current element being processed in the array.
        - index - The index of current element.
        - array - The array for which filter() was called upon.

    2. (optional) thisArg - A value to use as this when executing callback function. Default value is undefined.

2. Return: Returns new array of matching elements that satisfies that specified testing function. If no elements satisfies the testing function then it returns empty list.

3. Example:

        const array = [1,8,4,2];
        let divisibleBy4 = (element, index, array) => element % 4 === 0;
        array.filter(divisibleBy4); // [8,4]
        const array1 = [1,2,3];
        array1.filter(divisibleBy4); // Output: []
        console.log(array.filter((element) => element % 2 === 0)); // [8,4,2]

4. filter () accepts function and optional thisArg as parameters. Returns the elements of an array that meet the condition specified in a callback function. If no such element exists then it returns an empty array.

5. It doesn't mutate the original array.

## forEach ##

1. Parameters:

    1. Function - forEach () calls the callback function one time for each element in the array. This function accepts up to three arguments i.e.

        - element - The current element being processed in the array.
        - index - The index of current element.
        - array - The array for which forEach() was called upon.

    2. (optional) thisArg - A value to use as this when executing callback function. Default value is undefined.

2. Return: Returns undefined.

3. Example:

        let array = [1,2,3];
        // Function to call.
        squareElement = (element, index, array) => element**2;
        array.forEach(squareElement); // Output: undefined
        
        let array1 = [];
        array1.forEach(squareElement); // Output: undefined

        array.forEach((element) => {return element**3}); // Output: undefined

4. forEach () accepts function and optional thisArg as parameters. Performs the specified action for each element in an array. Returns undefined.

5. It doesn't mutate the original array.

## map ##

1. Parameters:

    1. Function - This function is called against each element. Returns evaluated value. This function accepts up to three arguments i.e.

        - element - The current element being processed in the array.
        - index - The index of current element.
        - array - The array for which map() was called upon.

    2. (optional) thisArg - A value to use as this when executing callback function. Default value is undefined.

2. Return: A new array with each element being the result of the callback function.

3. Example:

        let array = [1,2,3,4];
        // Function to call.
        squareArray = (element, index, array) => element**2;
        array.map(squareArray); // [1,4,9,16]
        let array1 = [];
        array1.map(squareArray); // Output: []
        array.map((element) => {return element**3}); // [1,8,27,64]

4. map () accepts function and optional thisArg as parameters. Calls a defined callback function on each element of an array, and returns an array that contains the results.

5. It doesn't mutate the original array.

## pop ##

1. Parameter: none.

2. Return: Removes last element of an array and returns it.

3. Example:

        const array = [1,2,3,4];
        array.pop(); // 4
        console.log(array); // [1,2,3]
        [7,8,9].pop(); // 9
        const array1 = [];
        array1.pop; // undefined

4. pop () removes last element of the array and returns that element. This method changes the length of the array. It returns undefined for an empty array.

5. It does mutate the original array.

## reduce ##

1. Parameters:

    1. Function - The reduce method calls the callback function one time for each element in the array. This function accepts up to four arguments i.e.

        - accumulator - The value resulting from the previous call to callbackFn. On first call, initialValue if specified, otherwise the value of array[0].

        - currentValue - The value of the current element. On first call, the value of array[0] if an initialValue was specified, otherwise the value of array[1].

        - index - The index position of currentValue in the array. On first call, 0 if initialValue was specified, otherwise 1.

        - array - The array for which reduce() was called upon.

    2. (optional) initialValue - If initialValue is specified, it is used as the initial value to start the accumulation. Otherwise the first element value will be used to start the accumulation.

2. Return: Returns a value that results from last call of the specified call back function.

3. Example:

        let array = [1,2,3,4];
        // Function to call.
        sumElements = (prev, curr, index, array) => curr + prev;
        array.reduce(sumElements, 0); // 10
        let array1 = [];
        array1.reduce(sumElements); // Throws error.
        [15, 16, 17, 18, 19].reduce((prev, curr, index, array) => prev + curr); // 85

4. reduce () accepts function and optional initialValue as parameters. Calls the specified callback function for all the elements in an array. The return value of the callback function is the accumulated result, and is provided as an argument in the next call to the callback function. After complete iteration, the accumulated result from the last call will be returned.

5. It doesn't mutate the original array.

## slice ##

1. Parameters:
    1. start (number) (optional)  - The beginning index where slicing starts from. If start is undefined, then the slice begins at index 0.
    2. end (number) (optional) - The ending index where slicing stops at. The element at the index 'end' is excluded. If end is undefined, then the slice extends to the end of the array.

2. Return: Copies portion of array according to the specified start and end values into a new array and returns the new array.

3. Example:

        const array = [1,2,3,4];
        array.slice(1,3); // [2,3]

        array.slice(-2); // [3,4]

        array.slice(); // [1,2,3,4]

4. slice() accepts two optional parameters i.e. start and end as mentioned above. Returns a copy of a section of an array into a new array selected from start to end (end not included) where start and end represent the index of items in that array. If start and end are not specified, it returns a full copy of array.

5. It doesn't mutate the original array.

## some ##

1. Parameters:

    1. Function - This function is called against each element. Used to check if any element returns true when passed to the specified function. This function accepts up to three arguments i.e.

        - element - The current element being processed in the array.
        - index (number) - The index of current element.
        - array - The array for which some() was called upon.

    2. (optional) thisArg - A value to use as this when executing callback function.

2. Return: Returns true if atleast one element passed to the specified function returns true otherwise returns false.

3. Example:

        let array = [1,2,3,4];
        // Function to call.
        let divisibleBy4 = (element, index, array) => element % 4 === 0;
        array.some(divisibleBy4); // Output: true
        let array1 = [1,2,3];
        array1.some(divisibleBy4); // Output: false
        array.some((element) => {return element % 2 === 0}); // Output: true.

4. some() accepts a callback function and an optional thisArg as parameters and determines whether the specified callback function returns true for any element of an array. Returns true if callback function returns true for any of the elements in the array, otherwise returns false.

5. It doesn't mutate the original array.
